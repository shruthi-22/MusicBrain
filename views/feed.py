from flask import Flask, Blueprint, render_template, session, request, redirect
import requests
import json

feed = Blueprint('feed', __name__, template_folder="templates",
                 static_folder="static", url_prefix='/feed')


@feed.route('/', methods=['GET'])
def generate_feedpage():
    if session.get('username')!=None:
        url = "https://musicbrain-backend.herokuapp.com/posts/feed?username="+session['username']
        response = requests.request("GET", url)
        response = json.loads(response.content)

        url = "https://musicbrain-backend.herokuapp.com/friends/following?username="+session['username']
        friends_response = requests.request("GET", url)
        friends_response = json.loads(friends_response.content)

        url = "https://musicbrain-backend.herokuapp.com/friends/count?username="+session['username']
        count_response = requests.request("GET", url)
        count_response = json.loads(count_response.content)

        url = "https://musicbrain-backend.herokuapp.com/songs/reco-playlist?username="+session['username']
        playlist_response = requests.request("GET", url)
        playlist_response = json.loads(playlist_response.content)

        url = "http://musicbrain-backend.herokuapp.com/friends/requested?username=" + \
            session['username']
        requested_response = requests.request("GET", url)
        requested_response = json.loads(requested_response.content)
        session['requested'] = requested_response['friends']
        print(session['requested'])

        if response['status'] and friends_response['status']:
            return render_template('index.html', username=session['username'],followers_count=count_response.get('followers')or'NA',
                                    following_count=count_response.get('following')or'NA',playlist_id=playlist_response.get('playlist_id') or "37i9dQZEVXbLZ52XmnySJg",
                                    posts=response['posts'],requested=session['requested'],friends=friends_response['friends'])
        else:
            return('<h2>Creation of feedpage failed.</h2><a href="/feed">Try again</a>')
    else:
        return redirect('/auth/')



